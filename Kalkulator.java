import java.util.*;
public class Kalkulator {
    public static void main (String[] args){
        int menu;
        boolean menuutama = true;
        do {
            System.out.println("\n-------------------------------------");
            System.out.println("Kalkulator Penghitung Luas dan Volume");
            System.out.println("-------------------------------------");
            System.out.println("1. Hitung Luas Bidang\n" +
                    "2. Hitung Volume\n" +
                    "0. Tutup Aplikasi");
            System.out.println("-------------------------------------");
            System.out.print("Masukkan pilihan: ");
            Scanner input = new Scanner(System.in);
            menu = input.nextInt();

            if (menu == 1) {
                boolean submenu = true;
                int pil1;
                do {
                    System.out.println("\n-------------------------------------");
                    System.out.println("   Pilih Bidang yang Akan Dihitung   ");
                    System.out.println("-------------------------------------");
                    System.out.println("1. Persegi Panjang\n" +
                            "2. Persegi \n" +
                            "3. Segitiga\n" +
                            "4. Lingkaran\n" +
                            "0. Kembali ke menu sebelumnya");
                    System.out.println("------------------------------------");
                    System.out.print("Masukkan pilihan  : ");
                    pil1 = input.nextInt();

                    switch (pil1) {
                        case 1:
                            int panjang, lebar, result;
                            System.out.println("\n-------------------------------------");
                            System.out.println("   Penghitung Luas Persegi Panjang   ");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan Panjang : ");
                            panjang = input.nextInt();
                            System.out.print("Masukkan Lebar   : ");
                            lebar = input.nextInt();
                            result = panjang * lebar;
                            System.out.println("Processing ...");
                            System.out.println("Luas Persegi Panjang adalah " + result + " cm^2\n");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan apa saja untuk lanjut : ");
                            pil1 = input.next().charAt(0);
                            System.out.println();
                            break;
                        case 2:
                            int sisi;
                            System.out.println("\n-------------------------------------");
                            System.out.println("       Penghitung Luas Persegi       ");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan Sisi    : ");
                            sisi = input.nextInt();
                            result = sisi * sisi;
                            System.out.println("Processing ...");
                            System.out.println("Luas Persegi adalah " + result + " cm^2\n");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan apa saja untuk lanjut : ");
                            pil1 = input.next().charAt(0);
                            System.out.println();
                            break;
                        case 3:
                            double alas, tinggi, results;
                            System.out.println("\n------------------------------------");
                            System.out.println("      Penghitung Luas Segitiga      ");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Alas     : ");
                            alas = input.nextInt();
                            System.out.print("Masukkan Tinggi   : ");
                            tinggi = input.nextInt();
                            System.out.println("Processing ...");
                            results = 0.5 * alas * tinggi;
                            System.out.println("Luas Segitiga adalah " + results + " cm^2\n");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan apa saja untuk lanjut : ");
                            pil1 = input.next().charAt(0);
                            System.out.println();
                            break;
                        case 4:
                            double jari, result1;
                            System.out.println("\n-------------------------------------");
                            System.out.println("      Penghitung Luas Lingkaran      ");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan Jari-Jari  : ");
                            jari = input.nextInt();
                            result1 = 3.14 * jari * jari;
                            System.out.println("Processing ...");
                            System.out.println("Luas Lingkaran adalah " + result1 + " cm^2\n");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan apa saja untuk lanjut : ");
                            pil1 = input.next().charAt(0);
                            System.out.println();
                            break;
                        case 0:
                            submenu = false;
                            break;
                        default:
                            System.out.println("Pilihan Tidak Tersedia Bro, Silakan pilih yang Ada :)\n");
                            break;
                    }
                } while (submenu);
            }
            else if (menu == 2) {
                boolean submenu2 = true;
                int pil2;
                do {
                    System.out.println("\n-------------------------------------");
                    System.out.println("   Pilih Bidang yang Akan Dihitung   ");
                    System.out.println("-------------------------------------");
                    System.out.println("1. Kubus\n" +
                            "2. Balok\n" +
                            "3. Tabung\n" +
                            "0. Kembali ke menu sebelumnya");
                    System.out.println("------------------------------------");
                    System.out.print("Masukkan pilihan: ");
                    pil2 = input.nextInt();
                    switch (pil2) {
                        case 1:
                            int sisi, result;
                            System.out.println("\n-------------------------------------");
                            System.out.println("       Penghitung Volume Kubus       ");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan Sisi   : ");
                            sisi = input.nextInt();
                            result = sisi * sisi * sisi;
                            System.out.println("Processing ...");
                            System.out.println("Volume Kubus adalah " + result + " cm^3\n");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan apa saja untuk lanjut : ");
                            pil2 = input.next().charAt(0);
                            System.out.println();
                            break;
                        case 2:
                            int panjang, lebar, tinggi;
                            System.out.println("\n-------------------------------------");
                            System.out.println("       Penghitung Volume Balok       ");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan Panjang : ");
                            panjang = input.nextInt();
                            System.out.print("Masukkan Lebar   : ");
                            lebar = input.nextInt();
                            System.out.print("Masukkan Tinggi  : ");
                            tinggi = input.nextInt();
                            result = panjang * lebar * tinggi;
                            System.out.println("Processing ...");
                            System.out.println("Volume Balok adalah " + result + " cm^3\n");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan apa saja untuk lanjut : ");
                            pil2 = input.next().charAt(0);
                            System.out.println();
                            break;
                        case 3:
                            int jari;
                            double result1;
                            System.out.println("\n------------------------------------");
                            System.out.println("      Penghitung Volume Tabung      ");
                            System.out.println("------------------------------------");
                            System.out.print("Masukkan Jari-Jari : ");
                            jari = input.nextInt();
                            System.out.print("Masukkan Tinggi    : ");
                            tinggi = input.nextInt();
                            result1 = 3.14 * tinggi * (jari * jari);
                            System.out.println("Processing ...");
                            System.out.println("Volume Tabung adalah " + result1 + "cm^3\n");
                            System.out.println("-------------------------------------");
                            System.out.print("Masukkan apa saja untuk lanjut : ");
                            pil2 = input.next().charAt(0);
                            System.out.println();
                            break;
                        case 0 :
                            submenu2 = false;
                            break;
                        default:
                            System.out.println("Pilihan Tidak Tersedia Sist, Silakan pilih yang Ada :)");
                            break;
                    }
                }while (submenu2);
            }
            else if (menu == 0) {
                System.out.println("Terima Kasih Telah Menggunakan Aplikasi ini Bung :)\n");
                System.exit(0);
            }
            else{
                System.out.println("Maaf Pilihan Tidak Tersedia Bosque :(");
            }
        }while (menuutama);
    }
}